from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
from .models import Content, Comment
from django.contrib.auth.decorators import login_required



@csrf_exempt
def get_content(request, wrench):
    if request.method == "PUT":
        value = request.body.decode('utf-8')
    elif request.method == "POST":
        action = request.POST['action']
        if action == "Send Content":
            value = request.POST['value']

    if request.method == "PUT" or (request.method == "POST" and action == "Send Content"):
        try:
            c = Content.objects.get(key=wrench)
            c.value = value
        except Content.DoesNotExist:
            c = Content(key=wrench, value=value)
        c.save()
    if request.method == "POST" and action == "Send Comment":
        c = get_object_or_404(Content, key=wrench)
        title = request.POST['title']
        body = request.POST['body']
        q = Comment(content=c, title=title, body=body, date=timezone.now())
        q.save()

    content = get_object_or_404(Content, key=wrench)
    context = {'content': content}
    return render(request, 'myapp_cms_users_put/content.html', context)


def index(request):
    content_list = Contenido.objects.all()[:5]
    context = {'content_list': content_list}
    return render(request, 'myapp_cms_users_put/index.html', context)


def loggedIn(request):
    if request.user.is_authenticated:
        logged = "Logged in as " + request.user.username
    else:
        logged = "Not logged in. <a href='/admin/'>Login via admin</a>"
    return HttpResponse(logged)


def logout_view(request):
    logout(request)
    return redirect("/myapp_cms_users_put/")


def image(request):
    template = loader.get_template('myapp_cms_users_put/template.html')
    context = {}
    return HttpResponse(template.render(context, request))

