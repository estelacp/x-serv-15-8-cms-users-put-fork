from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('image', views.image),
    path('loggedIn', views.loggedIn),
    path('logout', views.logout_view),
    path('<str:wrench>', views.get_content),
]



